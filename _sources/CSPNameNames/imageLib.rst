..  Copyright (C)  Mark Guzdial, Barbara Ericson, Briana Morrison, Jeff Elkner
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3 or
    any later version published by the Free Software Foundation; with
    Invariant Sections being Forward, Prefaces, and Contributor List,
    no Front-Cover Texts, and no Back-Cover Texts.  A copy of the license
    is included in the section entitled "GNU Free Documentation License".

.. |bigteachernote| image:: Figures/apple.jpg
    :width: 50px
    :align: top
    :alt: teacher note

.. 	qnum::
	:start: 1
	:prefix: csp-6-6-
	
.. highlight:: python 
   :linenothreshold: 4


Using an Image Library
========================

Similarly, in the image processing example, we used ``from PIL import Image``.
That made the ``Image`` object accessible. We could also define a new function
that returns a new color, or a new procedure that changes the image.  

.. note::

    Processing all the pixes in an image can take time, so please be patient
    after clicking the ``Run`` button as you await the results.

.. raw:: html

    <img src="../_static/arch.jpg" id="arch.jpg" >
    
.. activecode:: Image_Functions
    :nocodelens:

    # USE IMAGE LIBRARY 
    from PIL import Image 
    
    # CREATE IMAGE FROM A FILE
    img = Image.open("arch.jpg")

    # GET ITS DIMENSIONS
    width = img.size[0]
    height = img.size[1]

    # MODIFY ITS PIXEL COLOR
    pixels = img.load()
    for col in range(width):
        for row in range(height):
            r, g, b = pixels[col, row]          # GET PIXEL COLOR
            pixels[col, row] = (r * 0.5, g, b)  # HALF RED VALUE

    # SHOW CHANGED IMAGE
    img.show()


The ``for col in range(width)`` on line 13 and ``for row in range(height)`` on
line 14 let us loop through all of the pixels in the image and change the red
value for each pixel.  We'll talk more about looping (repeating steps) in the
next chapter.

.. mchoice:: 6_6_1_Image_Functions_Q1
   :answer_a: It sets the red value in the current pixel to half its previous value.  
   :answer_b: It sets the red value in the current pixel to twice its previous value.
   :answer_c: It leaves red value of the current pixel unchanged.
   :answer_d: It sets the red value in the current pixel to 5 times its previous value.  
   :correct: a
   :feedback_a: Multiplying by 0.5 is the same as dividing by 2.  
   :feedback_b: This would be true if it was r * 2, instead of r * 0.5
   :feedback_c: This would be true if it was r, instead of r * 0.5
   :feedback_d: This would be true if it was r *  5 instead of r * 0.5
   
   What does the line ``pixels[col, row] = (r * 0.5, g, b)`` do?


This ability to name functions and procedures, and sets of functions and
procedures, and absolutely anything and any set of things in a computer is very
powerful.  It allows us to create **abstractions** that make the computer
easier to program and use.  More on that in a future chapter.
