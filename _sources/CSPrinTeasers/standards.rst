..  Copyright (C)  Mark Guzdial, Barbara Ericson, Briana Morrison, Jeff Elkner
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3 or
    any later version published by the Free Software Foundation; with
    Invariant Sections being Forward, Prefaces, and Contributor List,
    no Front-Cover Texts, and no Back-Cover Texts.  A copy of the license
    is included in the section entitled "GNU Free Documentation License".

..  shortname:: Chapter: What You Can Do with a Computer
..  description:: Some tidbits of what you can do with a computer

.. setup for automatic question numbering.

.. qnum::
    :start: 1
    :prefix: csp-1-7-


.. |runbutton| image:: Figures/run-button.png
    :height: 20px
    :align: top
    :alt: run button

.. |audiobutton| image:: Figures/start-audio-tour.png
    :height: 20px
    :align: top
    :alt: audio tour button


Standards - Big Ideas
=====================

This book will address the following Big Ideas from Computer Science [#f1]_:

Big Idea 1: Creative Development
--------------------------------

* 1.1 Collaboration
* 1.2 Program Function and Purpose 
* 1.3 Program Design and Development 
* 1.4 Identifying and Correcting Errors 

Big Idea 2: Data 
----------------

* 2.1 Binary Numbers 
* 2.2 Data Compression 
* 2.3 Extracting Information from Data
* 2.4 Using Programs with Data
 
Big Idea 3: Algorithms and Programming 
--------------------------------------

* 3.1 Variables and Assignments 
* 3.2 Data Abstraction 
* 3.3 Mathematical Expressions
* 3.4 Strings
* 3.5 Boolean Expressions
* 3.6 Conditionals
* 3.7 Nested Conditionals
* 3.8 Iteration
* 3.9 Developing Algorithms
* 3.10 Lists
* 3.11 Binary Search
* 3.12 Calling Functions (a.k.a. Procedures)
* 3.13 Developing Functions (Procedures)
* 3.14 Libraries
* 3.15 Simulation
* 3.16 Algorithmic Efficiency
* 3.17 Undecidable Problems
 
Big Idea 4: Computer Systems and Networks 
-----------------------------------------

* 4.1 The Internet
* 4.2 Fault Tolerance
* 4.3 Parallel and Distributed Computing
 
Big Idea 5: Impact of Computing 
-------------------------------

* 5.1 Beneficial and Harmful Effects 
* 5.2 Digital Divide
* 5.3 Computing Bias
* 5.4 Crowdsourcing
* 5.5 Legal and Ethical Concerns
* 5.6 Safe Computing

This chapter should have given you a sense for what we are going to be doing
with a computer in this book.  Let's get started in the next chapter by talking
about what the computer can do and how you can control it.  

.. rubric:: Footnotes

.. [#f1] These standards were developed as part of the
   `AP Computer Science Principles
   <https://en.wikipedia.org/wiki/AP_Computer_Science_Principles>`_ curriculum.
