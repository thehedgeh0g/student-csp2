..  Copyright (C)  Mark Guzdial, Barbara Ericson, Briana Morrison, Jeff Elkner
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3 or
    any later version published by the Free Software Foundation; with
    Invariant Sections being Forward, Prefaces, and Contributor List,
    no Front-Cover Texts, and no Back-Cover Texts.  A copy of the license
    is included in the section entitled "GNU Free Documentation License".

.. qnum::
    :start: 1
    :prefix: csp-15-4-
	
Multiple If's with Images
=========================

.. index::
    single: posterize

We can use multiple if's to reduce the number of colors in an image.  Let's say
that if we have a little bit of each of red, green, and blue, we want to make
each of them zero.  If we have more, we set them to a mid-range value like 120.
This is called **posterizing** because it reduces all the colors in a picture
to a small number of colors -- like the ones you might use if you were making a
poster.

.. raw:: html

    <img src="../_static/arch.jpg" id="arch.jpg">
    
.. activecode:: Posterize
    :tour_1: "Structural Tour"; 1: id4/line1; 3: id4/line3; 6-7: id4/lines6n7; 8: id4/line8; 10-11: id4/lines10n11; 12-13: id4/lines12n13; 14-15: id4/lines14n15; 16-17: id4/lines16n17; 18-19: id4/lines18n19; 20-21: id4/lines20n21; 23: id4/line23; 25: id4/line25;
    :nocodelens:

    from PIL import Image 
    
    img = Image.open('arch.jpg')
    pixels = img.load()

    for x in range(img.size[0]):
        for y in range(img.size[1]):
            r, g, b = pixels[x, y]
            
            if r < 120:
                r = 0
            if r >= 120:
                r = 120
            if g < 120:
                g = 0
            if g >= 120:
                g = 120
            if b < 120:
                b = 0
            if b >= 120:
                b = 120
            
            pixels[x, y] = r, g, b
     
    img.show()
    
Rewrite the code for posterizing an image using if and else rather than
multiple if's. Test that it still works correctly. 

.. mchoice:: 15_4_1_posterize1
   :answer_a: 8
   :answer_b: 3
   :answer_c: 120
   :answer_d: 16,777,216 (= 256 * 256 * 256) 
   :correct: a
   :feedback_a: Two possible values of each of red, green, and blue (3 colors) is 2 raised to 3rd power combinations which is 8.
   :feedback_b: Two values of each of red, green, and blue is more than 3.
   :feedback_c: Far fewer
   :feedback_d: That's the total number of colors possible.  But this code reduces that.
   
   How many different colors will be in our image when we are done?
