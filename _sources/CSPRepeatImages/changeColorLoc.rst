..  Copyright (C)  Mark Guzdial, Barbara Ericson, Briana Morrison, Jeff Elkner
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3 or
    any later version published by the Free Software Foundation; with
    Invariant Sections being Forward, Prefaces, and Contributor List,
    no Front-Cover Texts, and no Back-Cover Texts.  A copy of the license
    is included in the section entitled "GNU Free Documentation License".
    
.. |audiobutton| image:: Figures/start-audio-tour.png
    :height: 20px
    :align: top
    :alt: audio tour button

.. qnum::
    :start: 1
    :prefix: csp-11-5-

Changing Step 6: Changing where we put the colors
=================================================

Below is a selection of images that you can use in the programs in this
section.

.. raw:: html

   <table>
   <tr><td>beach.jpg</td><td>baby.jpg</td><td>vangogh.jpg</td><td>swan.jpg</td></tr>
   <tr><td><img src="../_static/beach.jpg" id="beach.jpg"></td><td><img src="../_static/baby.jpg" id="baby.jpg"></td><td><img src="../_static/vangogh.jpg" id="vangogh.jpg"></td><td><img src="../_static/swan.jpg" id="swan.jpg"></td></tr>
   </table>
   <table>
   <tr><td>puppy.jpg</td><td>kitten.jpg</td><td>girl.jpg</td><td>motorcycle.jpg</td></tr>
   <tr><td><img src="../_static/puppy.jpg" id="puppy.jpg"></td><td><img src="../_static/kitten.jpg" id="kitten.jpg"></td><td><img src="../_static/girl.jpg" id="girl.jpg"></td><td><img src="../_static/motorcycle.jpg" id="motorcycle.jpg"></td></tr>
   </table>
   <table>
   <tr><td>gal1.jpg</td><td>guy1.jpg</td><td>gal2.jpg</td></tr>
   <tr><td><img src="../_static/gal1.jpg" id="gal1.jpg"></td><td><img src="../_static/guy1.jpg" id="guy1.jpg"></td><td><img src="../_static/gal2.jpg" id="gal2.jpg"></td></tr>
   </table>
   
We have been getting all of the pixels from an image using
``pixels = img.load()`` and then looping through all the pixels using:

.. sourcecode:: python

    for col in range(img.size[0]):
        for row in range(img.size[1]):

This processed all the pixels in the first column, then all the pixels in the
second column, and so on until it processed all the pixels in the image by
using two ``for`` loops with one inside of the other.  This is called a
**nested loop**. 

.. figure:: Figures/colOrder.png
    :align: center
    :figclass: align-center

    Figure 1: The order that pixels are processed if your outter loop is
    the column value.``

We can also loop through the pixels processing each row first by reversing
the outter and inner loops:

.. sourcecode:: python

    for row in range(img.size[1]):
        for col in range(img.size[0]):

Let's rename ``row`` as ``x`` and ``col`` as ``y`` to bring attention to how
each pixel value relates to an ``(x, y)`` value in a coordinate system.

Since the outer loop changes the  x (the column) and the inner loop changes the
y (the row), we will be processing all the pixels in the first row, then all
the pixels in the second row, and so on until we process all the pixels in the
image. The order doesn't matter if we are just setting the red to zero at every
pixel as shown below. 

.. figure:: Figures/rowOrder.png
    :align: center
    :figclass: align-center

    Figure 1: The order that pixels are processed if you use a nested loop with
    x changing in the outer loop and y changing in the inner loop.

.. activecode:: Nested_Loop_Clear_Red
    :tour_1: "Important Lines Tour"; 2: nli1-line2; 5: nli1-line5; 8: nli1-line8; 9: nli1-line9; 12: nli1-line12; 15: nli1-line15; 18: nli1-line18; 21-22: nli1-line21-22; 
    :nocodelens:

    # Step 1: Use the image library
    from PIL import Image 
    
    # Step 2: Connect to the image 
    img = Image.open('vangogh.jpg')

    # Step 3: Load the pixels
    pixels = img.load()

    # Step 4: Loop through the pixels
    for x in range(img.size[1]):
    	for y in range(img.size[0]):
        
            # Step 5: Get the data
            r, g, b = pixels[x, y]
            
            # Step 6: Modify the color
            pixels[x, y] = (0, g, b)
                        
    # Step 7: Show the result
    img.show()

We don't always have to modify the pixel color. Sometimes we can just change
the color at a different (x, y) location than the current pixel. This time we
will combine steps 5 (getting the color) with step 6 (modifying the color) to
get the pixel at (x, y) and copy the color from that pixel to the pixel at
location (y, x). So this will copy the color from (0, 2) to (2, 0). Later it
will also copy the color from (2, 0) to (0,2), since this loops through all the
pixels in the image, but that will just copy back the original color (why?). 

.. activecode:: Image_Location_Change
    :tour_1: "Important Lines Tour"; 2: nli2-line2; 5: nli2-line5; 8-9: nli2-line8-9; 12: nli2-line12; 15: nli2-line15; 18-19: nli2-line18-19;
    :nocodelens:

    # Step 1: Use the image library
    from PIL import Image 
    
    # Step 2: Connect to the image 
    img = Image.open('vangogh.jpg')

    # Step 3: Load the pixels 
    pixels = img.load()

    # Step 4: Loop through the pixels
    size = min(img.size[0], img.size[1])
    for x in range(size):
    	for y in range(size):
        
            # Steps 5 and 6: Get the data and modify the image
            pixels[x, y] = pixels[y, x]
            
    # Step 7: Show the result
    img.show()
    
Try the program above on some of the other images as well by changing the file
name on line 5. Does it always have the same effect?

.. mchoice:: 11_5_1_Image_YX_Q1
   :answer_a: We rotated the image 90 degrees to the left and flipped it over.
   :answer_b: We mirrored the image around a diagonal line from the top left to the bottom right.
   :answer_c: We flipped the image horizontally.
   :answer_d: No change.
   :correct: b
   :feedback_a: This would be true if we created a new image and set the values in the new image from the old pixel values.
   :feedback_b: This is true since we copy the pixel colors from the current image to the new location.
   :feedback_c: The pixels would be copied to the same row.
   :feedback_d: Compare this image to the original vangogh.jpg image.
   
   What happened when we copied the pixel color from (x, y) to (y, x)?

This one does a little math with the x and y.

.. activecode:: 11_5_2_Image_Flip_Both
    :tour_1: "Important Lines Tour"; 8-9: nli3-line8-9; 12: nli3-line12; 15: nli3-line15;
    :nocodelens:

    # Step 1: Use the image library
    from PIL import Image
    
    # Step 2: Connect to the image
    img = Image.open('vangogh.jpg')

    # Step 3: Load the pixels and get dimensions
    pixels = img.load()
    width = img.size[0]
    height = img.size[1]

    # Step 4: Loop through the pixel locations 
    for x in range(width):
    	for y in range(height):
    	
    	    # Step 5: Get the data
            r, g, b = pixels[x, y]
            
            # Step 6: Change the image
            pixels[width - 1 - x, height - 1 - y] = r, g, b 
            
    # Step 7: Show the result
    img.show()
    
Try the program above on some of the other images as well by changing the file
name on line 5. Which picture looks the best after you run the program?
